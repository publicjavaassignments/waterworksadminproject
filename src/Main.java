import javafx.application.Application;

import javafx.fxml.FXMLLoader;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.sql.SQLOutput;
import java.util.Scanner;

import Functionality.*;
public class Main extends Application
    {
        private Stage primaryStage;

        public static void main(String[] args)
            {
                Scanner in = new Scanner(System.in);
                ReminderHandler.main(); //
                System.out.println("do you want to use the gui or the console app?" +
                        "\n1 for gui" +
                        "\n2 for console");
                int consoleOrGui = in.nextInt();
                if (consoleOrGui==1) // launches gui version of app
                launch(args);
                if (consoleOrGui==2) // launches console verison of app
                ConsoleGUI();
            }

        @Override
        public void start(Stage primaryStage) throws Exception
            {
                String filePath = "db.properties";

                this.primaryStage = primaryStage;
               // this.primaryStage.getIcons().add(new Image("resources/icon.png"));

                Parent root = FXMLLoader.load(Main.class.getResource("GUI/MainApp.fxml"));
                primaryStage.setTitle("Waterworks Management System");
                primaryStage.setScene(new Scene(root));
                primaryStage.setResizable(false);
                primaryStage.show();
            }
            public static void ConsoleGUI(){
            Scanner in = new Scanner(System.in);
            ReminderHandler.main(); // this runs in background when program is run, will issue reminders at startup if dueDate have been crossed and set new duedate + add fees, etc.
                System.out.println("Please Select if you want to :" + // prints menu points
                        "\n1. Edit Client" +
                        "\n2. Create new Client" +
                        "\n3. Show Statistics" +
                        "\n4. Handle Payment" +
                        "\n5. Handle Readingcard");
                int choice = in.nextInt(); // gets the choice
                if (choice == 1){ // if edit client
                    System.out.println("Want to edit client by:" +
                            "\n1. CPR no" +
                            "\n2. CVR no" +
                            "\n3. Client ID");
                    int choice2 = in.nextInt(); // gets choice 2
                    if (choice2 ==1) { // if cpr no chosen
                        System.out.println("Please input the CPR NO");
                        int cprNo = in.nextInt(); // reads cprNo then executes below sequence of updates
                        ClientConfigurationHandler.updateByCprNo.updateClientName(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateClientEmail(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateClientPhoneNo(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateClientWaterStatus(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateSegmentType(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateAddress(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateZipCode(cprNo);
                        ClientConfigurationHandler.updateByCprNo.updateCprNo(cprNo);
                    }
                    else if(choice2==2){ // if cvr no chosen
                        System.out.println("Please input the CVR NO");
                        int cvrNo = in.nextInt(); // reads cvrNo then executes below sequence of updates
                        ClientConfigurationHandler.updateByCvrNo.updateClientName(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateClientEmail(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateClientPhoneNo(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateClientWaterStatus(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateSegmentType(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateAddress(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateCprNo(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateZipCode(cvrNo);
                        ClientConfigurationHandler.updateByCvrNo.updateCvrNo(cvrNo);
                    }
                    else if(choice2==3){ // if client id chosen
                        System.out.println("Please input the client ID");
                        int clientId = in.nextInt();// reads clientId then executes below sequence of updates
                        ClientConfigurationHandler.updateByClientId.updateClientName(clientId);
                        ClientConfigurationHandler.updateByClientId.updateClientEmail(clientId);
                        ClientConfigurationHandler.updateByClientId.updateClientPhoneNo(clientId);
                        ClientConfigurationHandler.updateByClientId.updateClientWaterStatus(clientId);
                        ClientConfigurationHandler.updateByClientId.updateSegmentType(clientId);
                        ClientConfigurationHandler.updateByClientId.updateAddress(clientId);
                        ClientConfigurationHandler.updateByClientId.updateCprNo(clientId);
                        ClientConfigurationHandler.updateByClientId.updateZipCode(clientId);
                        ClientConfigurationHandler.updateByClientId.updateCvrNo(clientId);
                    }


                }
                else if (choice == 2){ // if create new client chosen
                    ClientConfigurationHandler.createNewClient(); // starts sequence of creating new client
                }
                else if (choice == 3){ // if show statistics chosen
                    StatisticsHandler.main(); // starts sequence of statistics
                }
                else if (choice == 4){ // if Handle payment chosen
                    System.out.println("sorry, these functions havent been implemented yet.");
                }
                else if (choice == 5){// if handle readingcard chosen
                    System.out.println("sorry, these functions havent been implemented yet.");
                }
    }

    }
