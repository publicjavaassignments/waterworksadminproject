package Functionality;

import Functionality.Database.DB;

import java.util.Scanner;

public class ClientConfigurationHandler {
    public static void main(String[] args) {
        // PSVM is just for testing! Please remove it before integrating it into main.
        int clientId = getClientID(); //Asks for client ID to be used to search for the client in client updates
        int cprNo = getCprNo(); // Asks for client cpr number to be used to search for the client in the client updates
       int cvrNo = getCvrNo(); // Asks for client cvr number to be used to search for the client in the client updates
        createNewClient(); // creates new client

        //          FOLLOWING METHODS UPDATE A CLIENT BY USING THE CLIENT ID            \\
        updateByClientId.updateClientName(clientId); // updates client name
        updateByClientId.updateClientEmail(clientId); // updates client email
        updateByClientId.updateClientPhoneNo(clientId); // updates client phone number
        updateByClientId.updateClientWaterStatus(clientId); // updates client water status
        updateByClientId.updateSegmentType(clientId); // updates client segment type
        updateByClientId.updateCprNo(clientId); // updates client cpr number
        updateByClientId.updateCvrNo(clientId); // updates client cvr number
        updateByClientId.updateAddress(clientId); // updates client address
        updateByClientId.updateZipCode(clientId); // updates client zip code

        //          FOLLOWING METHODS UPDATE A CLIENT BY USING THE CVR NUMBER            \\
        updateByCvrNo.updateClientName(cvrNo); // updates client name
        updateByCvrNo.updateClientEmail(cvrNo); // updates client email
        updateByCvrNo.updateClientPhoneNo(cvrNo); // updates client phone number
        updateByCvrNo.updateClientWaterStatus(cvrNo); // updates client water status
        updateByCvrNo.updateSegmentType(cvrNo); // updates client segment type
        updateByCvrNo.updateCprNo(cvrNo); // updates client cpr number
        updateByCvrNo.updateAddress(cvrNo); // updates client address
        updateByCvrNo.updateZipCode(cvrNo); // updates client zip code
        updateByCvrNo.updateCvrNo(cvrNo); // updates client cvr number

        //          FOLLOWING METHODS UPDATE A CLIENT BY USING THE CPR NUMBER            \\
        updateByCprNo.updateClientName(cprNo); // updates client name
        updateByCprNo.updateClientEmail(cprNo); // updates client email
        updateByCprNo.updateClientPhoneNo(cprNo); // updates client phone number
        updateByCprNo.updateClientWaterStatus(cprNo); // updates client water status
        updateByCprNo.updateSegmentType(cprNo); // updates client segment type
        updateByCprNo.updateCvrNo(cprNo); // updates client cvr number
        updateByCprNo.updateAddress(cprNo); // updates client address
        updateByCprNo.updateZipCode(cprNo); // updates client zip code
        updateByCprNo.updateCprNo(cprNo); // updates client cpr number
    }
    public static void createNewClient() { // creates a new client in the database

        Scanner in = new Scanner(System.in);

        System.out.print("\nPlease enter the first name: ");
        String firstname = in.next(); //firstName.getText(); //reads first name for contact name for new user
        System.out.print("\nPlease enter the last name: ");
        String lastname = in.next(); //lastName.getText(); //reads last name for contact name for new user
        System.out.print("\nPlease enter the email: ");
        String email = in.next(); //email.getText(); //reads email for new user
        System.out.print("\nPlease enter the phone number: ");
        String phoneNo = in.next(); //phoneNo.getText(); //reads phone number for new user
        System.out.print("\nPlease enter the water status: ");
        int waterStatus = in.nextInt(); //waterStatus.getText(); //reads water status for new user
        System.out.print("\nPlease enter the segment type: ");
        String segmentType = in.next(); //segmentType.getText(); //reads segment type for new user
        System.out.print("\nPlease enter the cvr number: ");
        String cvrNo = in.next(); //cvrNo.getText(); //reads cvr number for new user
        System.out.print("\nPlease enter the cpr number: ");
        String cprNo = in.next(); //cprNo.getText(); //reads cpr number for new user
        System.out.print("\nPlease enter the street: ");
        String street = in.next(); //street.getText(); //reads street for new user address
        System.out.print("\nPlease enter the house number: ");
        String houseNumber = in.next(); //houseNo.getText(); //reads house number for new user address
        System.out.print("\nPlease enter the apartment: ");
        String apartment = in.next(); //apartment.getText(); //reads apartment site like - 1.t.v for new user address
        System.out.print("\nPlease enter the zip code ");
        int zipCode = in.nextInt(); //zipCode.getText(); //reads the Zip code for new user
        String contactName = firstname + " " + lastname; // combines the first and last name - the space would assign lastname to next field in DB if done in one variable.
        String address = street + " " + houseNumber + " " + apartment; // combines the addres, spaces would assign the next value to next field in DB if done in one variable.
        DB.insertSQL("INSERT INTO tblClient (fldContactName,fldEmail,fldPhoneNo,fldWaterStatus,fldSegmentType,fldCvrNo,fldCprNo,fldAddress,fldZipCode) " +
                "VALUES ('" + contactName + "','" + email + "','" + phoneNo + "'," + waterStatus + ",'" + segmentType + "','" + cvrNo + "','" + cprNo + "','" + address + "'," + zipCode + ")"); // inserts all the read values into DB
    } // creates a new client in DB

    private static int getClientID() {  // get client ID to be used in client searches
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the Client ID to modify");
        int clientId = in.nextInt();//clientId.getText();
        return clientId;
    } // this method gets the client id to be used

    private static int getCvrNo() { // get cvrNo to be used in client searches
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the Client CVR Number to modify");
        int cvrNo = in.nextInt();//cvrNo.getText();
        return cvrNo;
    } // this method gets the cvr number to be used

    private static int getCprNo() { // get cprNo to be used in client searches
        Scanner in = new Scanner(System.in);
        System.out.println("Please enter the Client CPR Number to modify");
        int cprNo = in.nextInt();//cprNo.getText();
        return cprNo;
    } // this method gets the cpr number to be used

    public static class updateByClientId {

        public static void updateClientName(int clientId) { // updates name in database
            Scanner in = new Scanner(System.in);
            System.out.print("\nPlease enter the new first name: ");
            String newfirstName = in.next(); //firstName.getText(); //reads first name
            System.out.print("\nPlease enter the new last name: ");
            String newlastName = in.next(); //lastName.getText(); //reads last name
            String newName = newfirstName+" "+newlastName; //newName.getText(); //reads new name for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldContactName ='" + newName + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateClientEmail(int clientId) { // updates email in database
            System.out.print("\nPlease enter the new email: ");
            Scanner in = new Scanner(System.in);
            String newEmail = in.next(); //newEmail.getText(); //reads new email for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldEmail ='" + newEmail + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateClientPhoneNo(int clientId) { // updates phone number in database
            System.out.print("\nPlease enter the new phone number: ");
            Scanner in = new Scanner(System.in);
            String newPhoneNo = in.next(); //newPhoneNo.getText(); //reads new phone number for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldPhoneNo ='" + newPhoneNo + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateClientWaterStatus(int clientId) { // updates water status in database
            System.out.print("\nPlease enter the new water status: ");
            Scanner in = new Scanner(System.in);
            int newWaterStatus = in.nextInt(); //newWaterStatus.getText(); //reads new water status for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldWaterStatus ='" + newWaterStatus + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateSegmentType(int clientId) { // updates segment type in database
            System.out.print("\nPlease enter the new segment type: ");
            Scanner in = new Scanner(System.in);
            String newSegmentType = in.next(); //newSegmentType.getText(); //reads new segment type for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldSegmentType ='" + newSegmentType + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateAddress(int clientId) { // updates address in database
            System.out.print("\nPlease enter the new address: ");
            Scanner in = new Scanner(System.in);
            System.out.print("\nPlease enter the street: ");
            String newStreet = in.next(); //street.getText(); //reads street for new user address
            System.out.print("\nPlease enter the house number: ");
            String newHouseNumber = in.next(); //houseNo.getText(); //reads house number for new user address
            System.out.print("\nPlease enter the apartment: ");
            String newApartment = in.next(); //apartment.getText(); //reads apartment site like - 1.t.v for new user address
            String newAddress =newStreet+" "+newHouseNumber+" "+newApartment; //newAddress.getText(); //reads new address for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldAddress ='" + newAddress + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateZipCode(int clientId) { // updates zipcode in database
            System.out.print("\nPlease enter the new zip code: ");
            Scanner in = new Scanner(System.in);
            String newZipCode = in.next(); //newZipCode.getText(); //reads new zip code for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldZipCode ='" + newZipCode + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateCvrNo(int clientId) { // updates cvrnumber in database
            System.out.print("\nPlease enter the new cvr number: ");
            Scanner in = new Scanner(System.in);
            String newCvrNo = in.next(); //newCvrNo.getText(); //reads new cvr for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldCvrNo ='" + newCvrNo + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }

        public static void updateCprNo(int clientId) { // updates cprnumber in database
            System.out.print("\nPlease enter the new cpr number: ");
            Scanner in = new Scanner(System.in);
            String newCvrNo = in.next(); //newCprNo.getText(); //reads new cpr for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldCprNo ='" + newCvrNo + "' WHERE fldClientId =" + clientId + ""); //inserts read information into DB
        }
    } // methods to update client by id

    public static class updateByCvrNo {
        public static void updateClientName(int cvrNo) { // updates name in database
            Scanner in = new Scanner(System.in);
            System.out.print("\nPlease enter the new first name: ");
            String newfirstName = in.next(); //firstName.getText(); //reads first name
            System.out.print("\nPlease enter the new last name: ");
            String newlastName = in.next(); //lastName.getText(); //reads last name
            String newName = newfirstName+" "+newlastName; //newName.getText(); //reads new name for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldContactName ='" + newName + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateClientEmail(int cvrNo) { // updates email in database
            System.out.print("\nPlease enter the new email: ");
            Scanner in = new Scanner(System.in);
            String newEmail = in.next(); //newEmail.getText(); //reads new email for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldEmail ='" + newEmail + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateClientPhoneNo(int cvrNo) { // updates phone number in database
            System.out.print("\nPlease enter the new phone number: ");
            Scanner in = new Scanner(System.in);
            String newPhoneNo = in.next(); //newPhoneNo.getText(); //reads new phone number for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldPhoneNo ='" + newPhoneNo + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateClientWaterStatus(int cvrNo) { // updates water status in database
            System.out.print("\nPlease enter the new water status: ");
            Scanner in = new Scanner(System.in);
            int newWaterStatus = in.nextInt(); //newWaterStatus.getText(); //reads new water status for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldWaterStatus ='" + newWaterStatus + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateSegmentType(int cvrNo) { // updates segment type in database
            System.out.print("\nPlease enter the new segment type: ");
            Scanner in = new Scanner(System.in);
            String newSegmentType = in.next(); //newSegmentType.getText(); //reads new segment type for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldSegmentType ='" + newSegmentType + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateAddress(int cvrNo) { // updates address in database
            System.out.print("\nPlease enter the new address: ");
            Scanner in = new Scanner(System.in);
            String newStreet = in.next(); //street.getText(); //reads street for new user address
            System.out.print("\nPlease enter the house number: ");
            String newHouseNumber = in.next(); //houseNo.getText(); //reads house number for new user address
            System.out.print("\nPlease enter the apartment: ");
            String newApartment = in.next(); //apartment.getText(); //reads apartment site like - 1.t.v for new user address
            String newAddress =newStreet+" "+newHouseNumber+" "+newApartment; //newAddress.getText(); //reads new address for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldAddress ='" + newAddress + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateZipCode(int cvrNo) { // updates zipcode in database
            System.out.print("\nPlease enter the new zip code: ");
            Scanner in = new Scanner(System.in);
            String newZipCode = in.next(); //newZipCode.getText(); //reads new zip code for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldZipCode ='" + newZipCode + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateCvrNo(int cvrNo) { // updates cvrnumber in database
            System.out.print("\nPlease enter the new cvr number: ");
            Scanner in = new Scanner(System.in);
            String newCvrNo = in.next(); //newCvrNo.getText(); //reads new cvr for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldCvrNo ='" + newCvrNo + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }

        public static void updateCprNo(int cvrNo) { // updates cprnumber in database
            System.out.print("\nPlease enter the new cpr number: ");
            Scanner in = new Scanner(System.in);
            String newCvrNo = in.next(); //newCprNo.getText(); //reads new cpr for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldCprNo ='" + newCvrNo + "' WHERE fldCvrNo =" + cvrNo + ""); //inserts read information into DB
        }
    } // methods to update client by cvr number

    public static class updateByCprNo {
        public static void updateClientName(int cprNo) { // updates name in database
            Scanner in = new Scanner(System.in);
            System.out.print("\nPlease enter the new first name: ");
            String newfirstName = in.next(); //firstName.getText(); //reads first name
            System.out.print("\nPlease enter the new last name: ");
            String newlastName = in.next(); //lastName.getText(); //reads last name
            String newName = newfirstName+" "+newlastName; //newName.getText(); //reads new name for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldContactName ='" + newName + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateClientEmail(int cprNo) { // updates email in database
            System.out.print("\nPlease enter the new email: ");
            Scanner in = new Scanner(System.in);
            String newEmail = in.next(); //newEmail.getText(); //reads new email for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldEmail ='" + newEmail + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateClientPhoneNo(int cprNo) { // updates phone number in database
            System.out.print("\nPlease enter the new phone number: ");
            Scanner in = new Scanner(System.in);
            String newPhoneNo = in.next(); //newPhoneNo.getText(); //reads new phone number for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldPhoneNo ='" + newPhoneNo + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateClientWaterStatus(int cprNo) { // updates water status in database
            System.out.print("\nPlease enter the new water status: ");
            Scanner in = new Scanner(System.in);
            int newWaterStatus = in.nextInt(); //newWaterStatus.getText(); //reads new water status for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldWaterStatus ='" + newWaterStatus + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateSegmentType(int cprNo) { // updates segment type in database
            System.out.print("\nPlease enter the new segment type: ");
            Scanner in = new Scanner(System.in);
            String newSegmentType = in.next(); //newSegmentType.getText(); //reads new segment type for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldSegmentType ='" + newSegmentType + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateAddress(int cprNo) { // updates address in database
            System.out.print("\nPlease enter the new address: ");
            Scanner in = new Scanner(System.in);
            String newStreet = in.next(); //street.getText(); //reads street for new user address
            System.out.print("\nPlease enter the house number: ");
            String newHouseNumber = in.next(); //houseNo.getText(); //reads house number for new user address
            System.out.print("\nPlease enter the apartment: ");
            String newApartment = in.next(); //apartment.getText(); //reads apartment site like - 1.t.v for new user address
            String newAddress =newStreet+" "+newHouseNumber+" "+newApartment; //newAddress.getText(); //reads new address for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldAddress ='" + newAddress + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateZipCode(int cprNo) { // updates zipcode in database
            System.out.print("\nPlease enter the new zip code: ");
            Scanner in = new Scanner(System.in);
            String newZipCode = in.next(); //newZipCode.getText(); //reads new zip code for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldZipCode ='" + newZipCode + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        private static void updateCvrNo(int cprNo) { // updates cvrnumber in database
            System.out.print("\nPlease enter the new cvr number: ");
            Scanner in = new Scanner(System.in);
            String newCvrNo = in.next(); //newCvrNo.getText(); //reads new cvr for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldCvrNo ='" + newCvrNo + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }

        public static void updateCprNo(int cprNo) { // updates cprnumber in database
            System.out.print("\nPlease enter the new cpr number: ");
            Scanner in = new Scanner(System.in);
            String newCvrNo = in.next(); //newCprNo.getText(); //reads new cpr for existing user
            DB.pendingData=false;
            DB.insertSQL("UPDATE tblClient SET fldCprNo ='" + newCvrNo + "' WHERE fldCprNo =" + cprNo + ""); //inserts read information into DB
        }
    } // methods to update client by cpr number
}
