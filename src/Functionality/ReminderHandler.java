package Functionality;

import Functionality.Database.DB;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class ReminderHandler
    {

        public static void main()
            {
                // PSVM is just for testing! Please remove it before integrating it into main.
               generateReminder(); // updates reminder NO, sets new due date and edits the price
            }

        private static void editPrice() {
            int count = 0; // initialize my counter with 0
            DB.selectSQL("SELECT fldPrice FROM tblBill"); // this first do while loop used to count my comming arrays lengths
            do {
                String price = DB.getDisplayData();
                if (price.equals(DB.NOMOREDATA)) {
                    break;
                } else {
                    count++; // increment the counter
                }
            } while(true) ;
            double [] priceArray = new double[count]; // array length determined from last do/while
            double [] reminderArray = new double[count]; // array length determined from last do/while
            int [] billIdArray = new int[count]; // array length determined from last do/while
            double [] feeArray = new double[3];
            feeArray[0]=50.0000;
            feeArray[1]=100.0000;
            feeArray[2]=150.0000;
            DB.selectSQL("SELECT fldPrice FROM tblBill");// this DB SELECT is used to get input to the price array
            count = 0; // reset the counter
            do {
                String price = DB.getDisplayData();
                if (price.equals(DB.NOMOREDATA)) {
                    break;
                } else {
                    priceArray[count] = Double.parseDouble(price); // sets the prices in the array
                    count++;
                }
            } while(true) ;

            DB.selectSQL("SELECT fldReminderNo FROM tblBill"); // this DB SELECT is used to get input to the reminderNo array
            count=0; // resets the counter
            do {
                String reminderNo = DB.getData();
                if (reminderNo.equals(DB.NOMOREDATA)) {
                    break;
                }
                if (reminderNo.equals("null")){ // if null replace with a 0 in array
                    reminderArray[count] = 0;
                count++; // increment the counter
                }
                else {
                    reminderArray[count]= Double.parseDouble(reminderNo); // sets the reminder no in the array
                    count++; // increment the counter
                }
            }while (true) ;

            DB.selectSQL("SELECT fldBillId FROM tblBill");// this DB SELECT is used to get input to the bill id array
            count = 0; // resets the counter
            do {
                String billId = DB.getData();
                if (billId.equals(DB.NOMOREDATA)) {
                    break;
                }
                if (billId.equals("null")){ // if null replace with a 0 in array
                    billIdArray[count] = 0;
                    count++; // increment the counter
                }
                else {
                    billIdArray[count]= Integer.parseInt(billId); // sets the reminder no in the array
                    count++; // increment the counter
                }
            }while (true) ;

            for (int i = 0; i < count ; i++) {
                DB.selectSQL("SELECT fldSegmentType FROM tblClient where fldClientId IN (SELECT fldClientId FROM tblBill WHERE fldBillId ="+billIdArray[i]+")");
                String segmentType = DB.getData();
                DB.pendingData=false;

                if (reminderArray[i]>=4){ // if segment is private add private fee for delayed cost
                    DB.updateSQL("UPDATE tblClient SET fldWaterStatus = 0 WHERE fldClientId IN (SELECT fldClientId FROM tblBill WHERE fldBillId ="+billIdArray[i]+")"); // shuts of water supply
                    if (segmentType.equalsIgnoreCase("private")){
                        DB.updateSQL("UPDATE tblBill SET fldPrice ="+(priceArray[i]+feeArray[0])+" WHERE fldBillId ="+billIdArray[i]+""); // adds the fee 50 for private segment
                    }
                    else if (segmentType.equalsIgnoreCase("agriculture")){
                        DB.updateSQL("UPDATE tblBill SET fldPrice ="+(priceArray[i]+feeArray[1])+" WHERE fldBillId ="+billIdArray[i]+""); // adds the fee 50 for agriculture segment
                    }
                    else if (segmentType.equalsIgnoreCase("Industry")){
                        DB.updateSQL("UPDATE tblBill SET fldPrice ="+(priceArray[i]+feeArray[2])+" WHERE fldBillId ="+billIdArray[i]+""); // adds the fee 50 for Industry segment
                    }
                }
                else if (segmentType.equalsIgnoreCase("private")){ // if segment is private add private fee for delayed cost
                    DB.updateSQL("UPDATE tblBill SET fldPrice ="+(priceArray[i]+feeArray[0])+" WHERE fldBillId ="+billIdArray[i]+""); // adds the fee 50 for private segment
                }
                else if (segmentType.equalsIgnoreCase("agriculture")){ // if segment is private add private fee for delayed cost
                    DB.updateSQL("UPDATE tblBill SET fldPrice ="+(priceArray[i]+feeArray[1])+" WHERE fldBillId ="+billIdArray[i]+""); // adds the fee 100 for agriculture segment
                }
                else if (segmentType.equalsIgnoreCase("industry")){ // if segment is private add private fee for delayed cost
                    DB.updateSQL("UPDATE tblBill SET fldPrice ="+(priceArray[i]+feeArray[2])+" WHERE fldBillId ="+billIdArray[i]+""); // adds the fee 150 for industry segment
                }
                DB.pendingData=true;
                System.out.println();
            }
            DB.pendingData=false;
        }

        private static void generateReminder() {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd"); // sets date format to match DB format
            LocalDate dateNow = LocalDate.now(); // gets date now on system now
            DB.selectSQL("SELECT fldBillId FROM tblBill");
            int count =0; // initialize counter
            do { // this do while is only used to get a counter with length of DB field to use in array length later
                String BillId = DB.getDisplayData(); // gets billId
                if ( BillId.equals(DB.NOMOREDATA)){
                    break;
                }
                else{
                    count++; // increments counter
                }
            }while (true);
            int [] bills = new int[count]; // array length derived from do while - array bills initialized
                DB.selectSQL("SELECT fldBillId FROM tblBill");
                count=0; // resets counter
                do {
                    String data = DB.getData(); // gets billId
                    if ( data.equals(DB.NOMOREDATA)){
                        break;
                    }
                    else{

                        bills[count]=Integer.parseInt(data); // sets billId in bills array at count

                    }
                    count++; // increments counter
                }while (true);

            for (int i = 0; i < count; i++) { // used to go trough all the billId's in array.
                int billId = bills[i];
                DB.selectSQL("SELECT fldDueDate FROM tblBill where fldBillId ="+billId+"");
                LocalDate readDate = LocalDate.parse(DB.getData());
                if(dateNow.isAfter(readDate)) { // compares duedate and current time, if true continue generating a reminder


                    String data = "";
                    DB.selectSQL("SELECT fldReminderNo FROM tblBill WHERE fldBillId = '"+billId+"'");
                    data = DB.getData();
                    DB.pendingData=false;
                    int newReminder = 0; // default value for new reminder value
                    if (data.equals("null")) { // if reminder value in DB currently = NULL sets it to 1 if date elapsed
                        newReminder = 1;
                       editPrice(); // calls the editPrice method to add reminder fees
                        editDueDate(); // updates due date
                    } else { // if reminder 1 or bigger increment it by 1 if date elapsed
                        newReminder = Integer.parseInt(data) + 1;
                       editPrice(); // calls the editPrice method to add reminder fees
                        editDueDate(); // updates due date
                    }
                    System.out.println();
                    DB.updateSQL("UPDATE tblBill SET fldReminderNo =" + newReminder+" WHERE fldBillId ="+billId+""); // updates the reminder id in DB
                    DB.moreData=false;
                }
            }



                }

        private static void editDueDate() { // sets a new due date if a reminder have been issued
            int count = 0; // initialize counter
            DB.selectSQL("SELECT fldBillId FROM tblBill");
            do { // this is used to count length of field billId
                String BillId = DB.getDisplayData();
                if ( BillId.equals(DB.NOMOREDATA)){
                    break;
                }
                else{
                    count++; // increments counter
                }
            }while (true);
            int [] bills = new int[count]; // bills array initilized with array length of count
            DB.pendingData=false;
            DB.selectSQL("SELECT fldBillId FROM tblBill");
            count=0; // resets counter
            do { // this do while will set the date into the array
                String data = DB.getData();
                if ( data.equals(DB.NOMOREDATA)){
                    break;
                }
                else{

                    bills[count]=Integer.parseInt(data); // dates set stored into array

                }
                count++; // counter incremented
            }while (true);

            for (int i = 0; i < count; i++) { // this fori loop


                int billId = bills[i];
                DB.selectSQL("SELECT fldDueDate FROM tblBill where fldBillId =" + bills[i] + "");
                LocalDate readDate = LocalDate.parse(DB.getData());
                LocalDate newDate = readDate.plusDays(7); // this increments the overdue date with 7 days to new payment
                DB.pendingData=false;
                DB.updateSQL("UPDATE tblBill SET fldDueDate ='"+newDate+"' WHERE fldBillId ="+bills[i]+""); // this inputs the new overduedate into DB
            }
        }
    }
