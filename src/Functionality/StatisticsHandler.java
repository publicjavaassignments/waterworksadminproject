package Functionality;

import Functionality.Database.DB;

// Show payment totals
// Show payment totals across segments

// Show total consumption
// Show total consumption across segments

public class StatisticsHandler
    {

        // Consumption Methods
        public static String showTotalConsumption() // gets total consumption across all segments
            {
                Functionality.Database.DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='private')");
                String totalConsumption = Functionality.Database.DB.getDisplayData();
                return totalConsumption;
            }

        public static double showTotalPrivateConsumption() // gets total consumption from private segment
            {
                DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(SELECT fldClientId FROM tblClient WHERE fldSegmentType ='private')");
                String totalPrivateConsumption = Functionality.Database.DB.getData();
                double totalPrivateConsumptionCast = 0;
                if (totalPrivateConsumption.equalsIgnoreCase("null")){
                    totalPrivateConsumptionCast = 0.00;
                }else {
                    totalPrivateConsumptionCast = Double.parseDouble(totalPrivateConsumption);
                }
                return totalPrivateConsumptionCast;
            }
        public static double showTotalAgricultureConsumption()// gets total consumption from agriculture segment
        {
            DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(SELECT fldClientId FROM tblClient WHERE fldSegmentType ='agriculture')");
            String totalagricultureConsumption = Functionality.Database.DB.getData();
            double totalagricultureConsumptionCast = 0;
            if (totalagricultureConsumption.equalsIgnoreCase("null")){
                totalagricultureConsumptionCast = 0.00;
        }else {
                totalagricultureConsumptionCast = Double.parseDouble(totalagricultureConsumption);
            }
            return totalagricultureConsumptionCast;
        }
        public static double showTotalIndustryConsumption()// gets total consumption from industry segment
        {
            DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(SELECT fldClientId FROM tblClient WHERE fldSegmentType ='Industry')");
            String totalIndustryConsumption = Functionality.Database.DB.getData();
            double totalIndustryConsumptionCast = 0;
            if (totalIndustryConsumption.equalsIgnoreCase("null")){
                totalIndustryConsumptionCast = 0.00;
            }else {
                totalIndustryConsumptionCast = Double.parseDouble(totalIndustryConsumption);
            }
            return totalIndustryConsumptionCast;
        }

        // Payment Methods
        public static String showTotalPayments() // gets total payments
            {
                DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill");
                String totalPayments = Functionality.Database.DB.getDisplayData();
                return totalPayments;
            }

        public static double showTotalPrivatePayments()// gets total payments from private segment
        {
            DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill WHERE fldClientId IN(SELECT fldClientId FROM tblClient WHERE fldSegmentType ='private')");
            String totalPrivateConsumption = Functionality.Database.DB.getData();
            double totalPrivateConsumptionCast = 0;
            if (totalPrivateConsumption.equalsIgnoreCase("null")){
                totalPrivateConsumptionCast = 0.00;
            }else {
                totalPrivateConsumptionCast = Double.parseDouble(totalPrivateConsumption);
            }
            return totalPrivateConsumptionCast;
        }
        public static double showTotalAgriculturePayments()// gets total payments from agriculture segment
        {
            DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill WHERE fldClientId IN(SELECT fldClientId FROM tblClient WHERE fldSegmentType ='agriculture')");
            String totalagricultureConsumption = Functionality.Database.DB.getData();
            double totalagricultureConsumptionCast = 0;
            if (totalagricultureConsumption.equalsIgnoreCase("null")){
                totalagricultureConsumptionCast = 0.00;
            }else {
                totalagricultureConsumptionCast = Double.parseDouble(totalagricultureConsumption);
            }
            return totalagricultureConsumptionCast;
        }
        public static double showTotalIndustryPayments()// gets total payments from industry segment
        {
            DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill WHERE fldClientId IN(SELECT fldClientId FROM tblClient WHERE fldSegmentType ='Industry')");
            String totalIndustryConsumption = Functionality.Database.DB.getData();
            double totalIndustryConsumptionCast = 0;
            if (totalIndustryConsumption.equalsIgnoreCase("null")){
                totalIndustryConsumptionCast = 0.00;
            }else {
                totalIndustryConsumptionCast = Double.parseDouble(totalIndustryConsumption);
            }
            return totalIndustryConsumptionCast;
        }

        public static void main()
            {
                System.out.print("\nTotal Consumption : ");
                System.out.print(showTotalConsumption());
                System.out.print("\nTotal Private Consumption : ");
                System.out.print(showTotalPrivateConsumption());
                System.out.print("\nTotal Agriculture Consumption : ");
                System.out.print(showTotalAgricultureConsumption());
                System.out.print("\nTotal Industry Consumption : ");
                System.out.print(showTotalIndustryConsumption());
                System.out.print("\nTotal Payments : ");
                System.out.print(showTotalPayments());
                System.out.print("\nTotal Private Payments : ");
                System.out.print(showTotalPrivatePayments());
                System.out.print("\nTotal Agriculture Payments : ");
                System.out.println(showTotalAgriculturePayments());
                System.out.print("\nTotal Industry Payments : ");
                System.out.print(showTotalIndustryPayments());
            }
    }
