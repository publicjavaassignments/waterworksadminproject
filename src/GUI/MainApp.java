package GUI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp
    {
        private Stage primaryStage;

        @FXML
        private ResourceBundle resources;

        @FXML
        private URL location;

        @FXML
        void initialize()
            {

            }


        public void showStatistics() throws IOException
            {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("ShowStatistics.fxml"));
                AnchorPane statisticsPane = loader.load();


                Stage dialogStage = new Stage();
                dialogStage.setTitle("Statistical Overview");
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.initOwner(primaryStage);
                Scene scene = new Scene(statisticsPane);
                dialogStage.setScene(scene);
                dialogStage.setResizable(false);

                loader.getController();

                dialogStage.showAndWait();
            }

        public void showClientHandler() throws IOException
            {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(MainApp.class.getResource("ClientHandler.fxml"));
                AnchorPane clientHandlerPane = loader.load();

                Stage dialogStage = new Stage();
                dialogStage.setTitle("Client Management");
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.initOwner(primaryStage);
                Scene scene = new Scene(clientHandlerPane);
                dialogStage.setScene(scene);
                dialogStage.setResizable(false);

                loader.getController();

                dialogStage.showAndWait();
            }

        public void exitApplication()
            {
                System.exit(0);
            }
    }
