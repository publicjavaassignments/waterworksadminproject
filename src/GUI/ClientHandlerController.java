package GUI;

import Functionality.Database.DB;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class ClientHandlerController
    {
        // WRITEABLE LABELS
        @FXML
        Label clientNameID = null;
        @FXML
        Label clientAddressID = null;
        @FXML
        Label clientZipcodeID = null;
        @FXML
        Label clientSegmentID = null;
        @FXML
        Label clientWaterlineID = null;
        @FXML
        Label clientContactNumberID = null;
        @FXML
        Label clientEmailID = null;
        @FXML
        Label clientWaterlineID_DisableTab = null;

        // Input Textfields
        @FXML
        TextField clientID_FXID = null;
        @FXML
        TextField ClientCVRID = null;
        @FXML
        TextField ClientCPRID = null;

        // Input textfields for update/delete
        @FXML
        TextField txtfieldAddress = null;
        @FXML
        TextField txtfieldZipCode = null;
        @FXML
        TextField txtfieldName = null;
        @FXML
        TextField txtfieldEmail = null;
        @FXML
        TextField txtfieldNumber = null;
        @FXML
        TextField txtfieldSegment = null;

        public void enableWater()
            {
                if (clientID_FXID.getText().isEmpty() && ClientCVRID.getText().isEmpty()) // Search for CPR
                    {
                        String CPRinputString = ClientCPRID.getText();
                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldWaterStatus = '1' where fldCprNo = '" + CPRinputString + "'");
                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        searchClient();
                        //Show on water change
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Water enabled");
                        newCustomerCreated.setHeaderText("The water has been enabled!");
                        newCustomerCreated.showAndWait();


                    } else if (clientID_FXID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for CVR
                    {
                        String CVRinputString = ClientCVRID.getText();
                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldWaterStatus = '1' where fldCvrNo = '" + CVRinputString + "'");
                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        searchClient();
                        //Show on water change
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Water enabled");
                        newCustomerCreated.setHeaderText("The water has been enabled!");
                        newCustomerCreated.showAndWait();

                    } else if (ClientCVRID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for ID
                    {
                        String IDinputString = clientID_FXID.getText();
                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldWaterStatus = '1' where fldClientId = '" + IDinputString + "'");
                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        searchClient();
                        //Show on water change
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Water enabled");
                        newCustomerCreated.setHeaderText("The water has been enabled!");
                        newCustomerCreated.showAndWait();

                    }
            }

        public void disableWater()
            {
                if (clientID_FXID.getText().isEmpty() && ClientCVRID.getText().isEmpty()) // Search for CPR
                    {
                        String CPRinputString = ClientCPRID.getText();
                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldWaterStatus = '0' where fldCprNo = '" + CPRinputString + "'");
                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        //Show on water change
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Water disabled");
                        newCustomerCreated.setHeaderText("The water has been disabled!");
                        newCustomerCreated.showAndWait();

                    } else if (clientID_FXID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for CVR
                    {
                        String CVRinputString = ClientCVRID.getText();
                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldWaterStatus = '0' where fldCvrNo = '" + CVRinputString + "'");
                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        //Show on water change
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Water disabled");
                        newCustomerCreated.setHeaderText("The water has been disabled!");
                        newCustomerCreated.showAndWait();

                    } else if (ClientCVRID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for ID
                    {
                        String IDinputString = clientID_FXID.getText();
                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldWaterStatus = '0' where fldClientId = '" + IDinputString + "'");
                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        //Show on water change
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Water disabled");
                        newCustomerCreated.setHeaderText("The water has been disabled!");
                        newCustomerCreated.showAndWait();

                    }
            }

        public void updateClient()
            {
                if (clientID_FXID.getText().isEmpty() && ClientCVRID.getText().isEmpty()) // Search for CPR
                    {
                        String CPRinputString = ClientCPRID.getText();

                        String updateFieldAddress = txtfieldAddress.getText();
                        String updateFieldZipcode = txtfieldZipCode.getText();
                        String updateFieldName = txtfieldName.getText();
                        String updateFieldEmail = txtfieldEmail.getText();
                        String updateFieldPhoneNo = txtfieldNumber.getText();
                        String updateFieldSegment = txtfieldSegment.getText();

                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldAddress = '" + updateFieldAddress + "', fldZipCode = '" + updateFieldZipcode + "', fldContactName = '" + updateFieldName + "', fldEmail = '" + updateFieldEmail + "', fldPhoneNo = '" + updateFieldPhoneNo + "', fldSegmentType = '" + updateFieldSegment + "' where fldCprNo = '" + CPRinputString + "'");

                        //Show on update
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Client updated!");
                        newCustomerCreated.setHeaderText("The client has been updated.");
                        newCustomerCreated.showAndWait();

                    } else if (clientID_FXID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for CVR
                    {
                        String CVRinputString = ClientCVRID.getText();

                        String updateFieldAddress = txtfieldAddress.getText();
                        String updateFieldZipcode = txtfieldZipCode.getText();
                        String updateFieldName = txtfieldName.getText();
                        String updateFieldEmail = txtfieldEmail.getText();
                        String updateFieldPhoneNo = txtfieldNumber.getText();
                        String updateFieldSegment = txtfieldSegment.getText();

                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldAddress = '" + updateFieldAddress + "', fldZipCode = '" + updateFieldZipcode + "', fldContactName = '" + updateFieldName + "', fldEmail = '" + updateFieldEmail + "', fldPhoneNo = '" + updateFieldPhoneNo + "', fldSegmentType = '" + updateFieldSegment + "' where fldCvrNo = '" + CVRinputString + "'");

                        //Show on update
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Client updated!");
                        newCustomerCreated.setHeaderText("The client has been updated.");
                        newCustomerCreated.showAndWait();

                    } else if (ClientCVRID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for ID
                    {
                        String IDinputString = clientID_FXID.getText();

                        String updateFieldAddress = txtfieldAddress.getText();
                        String updateFieldZipcode = txtfieldZipCode.getText();
                        String updateFieldName = txtfieldName.getText();
                        String updateFieldEmail = txtfieldEmail.getText();
                        String updateFieldPhoneNo = txtfieldNumber.getText();
                        String updateFieldSegment = txtfieldSegment.getText();

                        Functionality.Database.DB.selectSQL("UPDATE tblClient SET fldAddress = '" + updateFieldAddress + "', fldZipCode = '" + updateFieldZipcode + "', fldContactName = '" + updateFieldName + "', fldEmail = '" + updateFieldEmail + "', fldPhoneNo = '" + updateFieldPhoneNo + "', fldSegmentType = '" + updateFieldSegment + "' where fldClientId = '" + IDinputString + "'");

                        //Show on update
                        Alert newCustomerCreated = new Alert(Alert.AlertType.INFORMATION);
                        newCustomerCreated.setTitle("Client updated!");
                        newCustomerCreated.setHeaderText("The client has been updated.");
                        newCustomerCreated.showAndWait();

                    }
            }

        public void deleteClient()
            {
                if (clientID_FXID.getText().isEmpty() && ClientCVRID.getText().isEmpty()) // Search for CPR
                    {
                        String CPRinputString = ClientCPRID.getText();
                        Functionality.Database.DB.selectSQL("delete from tblClient where fldCprNo = '" + CPRinputString + "'");

                        //Show on delete
                        Alert newCustomerCreated = new Alert(Alert.AlertType.WARNING);
                        newCustomerCreated.setTitle("Client deleted!");
                        newCustomerCreated.setHeaderText("The client has been deleted.");
                        newCustomerCreated.showAndWait();

                    } else if (clientID_FXID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for CVR
                    {
                        String CVRinputString = ClientCVRID.getText();
                        Functionality.Database.DB.selectSQL("delete from tblClient where fldCprNo = '" + CVRinputString + "'");

                        //Show on delete
                        Alert newCustomerCreated = new Alert(Alert.AlertType.WARNING);
                        newCustomerCreated.setTitle("Client deleted!");
                        newCustomerCreated.setHeaderText("The client has been deleted.");
                        newCustomerCreated.showAndWait();

                    } else if (ClientCVRID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for ID
                    {
                        String IDinputString = clientID_FXID.getText();
                        Functionality.Database.DB.selectSQL("delete from tblClient where fldCprNo = '" + IDinputString + "'");

                        //Show on delete
                        Alert newCustomerCreated = new Alert(Alert.AlertType.WARNING);
                        newCustomerCreated.setTitle("Client deleted!");
                        newCustomerCreated.setHeaderText("The client has been deleted.");
                        newCustomerCreated.showAndWait();

                    }
            }

        public void createClient()
            {

            }

        public void searchClient()
            {
                // What are methods even lol
                if (clientID_FXID.getText().isEmpty() && ClientCVRID.getText().isEmpty()) // Search for CPR
                    {
                        String CPRinputString = ClientCPRID.getText();
                        clientID_FXID.setText("");
                        ClientCVRID.setText("");

                        Functionality.Database.DB.selectSQL("SELECT fldContactName from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientNameString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldAddress from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientAddressString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldZipCode from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientZipcodeString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldSegmentType from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientSegmentString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldPhoneNo from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientContactNumberString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldEmail from tblClient where fldCprNo = '" + CPRinputString + "'");
                        String clientEmailString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("Has water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        clientEmailID.setText(clientEmailString);
                        clientContactNumberID.setText(clientContactNumberString);
                        clientSegmentID.setText(clientSegmentString);
                        clientZipcodeID.setText(clientZipcodeString);
                        clientAddressID.setText(clientAddressString);
                        clientNameID.setText(clientNameString);

                        txtfieldAddress.setText(clientAddressString);
                        txtfieldZipCode.setText(clientZipcodeString);
                        txtfieldName.setText(clientNameString);
                        txtfieldEmail.setText(clientEmailString);
                        txtfieldNumber.setText(clientContactNumberString);
                        txtfieldSegment.setText(clientSegmentString);

                    } else if (clientID_FXID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for CVR
                    {

                        String CVRinputString = ClientCVRID.getText();
                        clientID_FXID.setText("");
                        ClientCPRID.setText("");

                        Functionality.Database.DB.selectSQL("SELECT fldContactName from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientNameString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldAddress from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientAddressString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldZipCode from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientZipcodeString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldSegmentType from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientSegmentString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldPhoneNo from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientContactNumberString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldEmail from tblClient where fldCvrNo = '" + CVRinputString + "'");
                        String clientEmailString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("Has water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        clientEmailID.setText(clientEmailString);
                        clientContactNumberID.setText(clientContactNumberString);
                        clientSegmentID.setText(clientSegmentString);
                        clientZipcodeID.setText(clientZipcodeString);
                        clientAddressID.setText(clientAddressString);
                        clientNameID.setText(clientNameString);

                        txtfieldAddress.setText(clientAddressString);
                        txtfieldZipCode.setText(clientZipcodeString);
                        txtfieldName.setText(clientNameString);
                        txtfieldEmail.setText(clientEmailString);
                        txtfieldNumber.setText(clientContactNumberString);
                        txtfieldSegment.setText(clientSegmentString);

                    } else if (ClientCVRID.getText().isEmpty() && ClientCPRID.getText().isEmpty()) // Search for ID
                    {

                        String IDinputString = clientID_FXID.getText();
                        ClientCVRID.setText("");
                        ClientCPRID.setText("");

                        Functionality.Database.DB.selectSQL("SELECT fldContactName from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientNameString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldAddress from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientAddressString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldZipCode from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientZipcodeString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldSegmentType from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientSegmentString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldWaterStatus from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientWaterlineString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldPhoneNo from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientContactNumberString = DB.getDisplayData();

                        Functionality.Database.DB.selectSQL("SELECT fldEmail from tblClient where fldClientId = '" + IDinputString + "'");
                        String clientEmailString = DB.getDisplayData();

                        // Write fancy water status
                        Double waterLineBoolean = Double.parseDouble(clientWaterlineString);

                        if (waterLineBoolean == 0)
                            {
                                clientWaterlineID.setText("No water");
                                clientWaterlineID_DisableTab.setText("No water");
                            } else
                            {
                                clientWaterlineID.setText("Has water");
                                clientWaterlineID_DisableTab.setText("Has water");
                            }

                        clientEmailID.setText(clientEmailString);
                        clientContactNumberID.setText(clientContactNumberString);
                        clientSegmentID.setText(clientSegmentString);
                        clientZipcodeID.setText(clientZipcodeString);
                        clientAddressID.setText(clientAddressString);
                        clientNameID.setText(clientNameString);

                        txtfieldAddress.setText(clientAddressString);
                        txtfieldZipCode.setText(clientZipcodeString);
                        txtfieldName.setText(clientNameString);
                        txtfieldEmail.setText(clientEmailString);
                        txtfieldNumber.setText(clientContactNumberString);
                        txtfieldSegment.setText(clientSegmentString);

                    }
            }

        @FXML
        void initialize()
            {

            }
    }
