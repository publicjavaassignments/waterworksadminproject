package GUI;

import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;

public class LoginController implements Initializable
    {
        public static void main(String[] args) {

        }
        public TextField hostname_txtID;
        public TextField port_txtID;
        public TextField username_txtID;
        public TextField password_TXTID;

        Button login_buttonID = new Button("LOGIN");

        @Override
        public void initialize(URL url, ResourceBundle rb)
            {

            }

        public void exitApplication()
            {
                System.exit(0);
            }

        public void handleDatabaseLogin() throws IOException
            {
                String hostname = hostname_txtID.getText();
                String port = port_txtID.getText();
                String username = username_txtID.getText();
                String password = password_TXTID.getText();
                System.out.println("Logging into server " + hostname + ":" + port);
                System.out.println("As user: " + username + " using password " + "**********");

                Path path = Paths.get("db.properties");

                try (BufferedWriter writer = Files.newBufferedWriter(path))
                    {
                        writer.write("port=" + port);
                        writer.write("\n");
                        writer.write("databaseName=" + hostname);
                        writer.write("\n");
                        writer.write("userName=" + username);
                        writer.write("\n");
                        writer.write("password=" + password);
                    }
            }
    }
