package GUI;

import java.net.URL;
import java.util.ResourceBundle;

import Functionality.Database.DB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

public class ShowStatisticsController implements Initializable
    {
        @FXML
        Label segmentWaterPercentileID = null;
        @FXML
        Label segmentPaymentPercentileID = null;

        @FXML
        Label totalWaterConsumptionID = null;
        @FXML
        Label privateWaterConsumptionID = null;
        @FXML
        Label industrialWaterConsumptionID = null;
        @FXML
        Label agriculturalWaterConsumptionID = null;

        @FXML
        Label totalPaymentsID = null;
        @FXML
        Label privatePaymentsID = null;
        @FXML
        Label industrialPaymentsID = null;
        @FXML
        Label agriculturalPaymentsID = null;

        @FXML
        PieChart consumptionStatistics;
        @FXML
        PieChart paymentStatistics;

        @Override
        public void initialize(URL url, ResourceBundle rb)
            {

                // Ugly SQL injections & casts
                DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='private')");
                String totalPrivateConsumption = Functionality.Database.DB.getDisplayData();
                double totalPrivateConsumptionCast = Double.parseDouble(totalPrivateConsumption);

                DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='industry')");
                String totalIndustryConsumption = Functionality.Database.DB.getDisplayData();
                double totalIndustryConsumptionCast = Double.parseDouble(totalIndustryConsumption);

                DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='agriculture')");
                String totalAgricultureConsumption = Functionality.Database.DB.getDisplayData();
                double totalAgricultureConsumptionCast = Double.parseDouble(totalAgricultureConsumption);

                ObservableList<PieChart.Data> pieChartConsumptionData =
                        FXCollections.observableArrayList(
                                new PieChart.Data("Private", totalPrivateConsumptionCast),
                                new PieChart.Data("Industry", totalIndustryConsumptionCast),
                                new PieChart.Data("Agriculture", totalAgricultureConsumptionCast));

                DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='private')");
                String totalPrivatePayment = Functionality.Database.DB.getDisplayData();
                double totalPrivatePaymentCast = Double.parseDouble(totalPrivatePayment);

                DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='industry')");
                String totalIndustryPayment = Functionality.Database.DB.getDisplayData();
                double totalIndustryPaymentCast = Double.parseDouble(totalIndustryPayment);

                DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill WHERE fldClientId IN(\n" +
                        "SELECT fldClientId FROM tblClient WHERE fldSegmentType ='agriculture')");
                String totalAgriculturePayment = Functionality.Database.DB.getDisplayData();
                double totalAgriculturePaymentCast = Double.parseDouble(totalAgriculturePayment);

                ObservableList<PieChart.Data> pieChartPaymentData =
                        FXCollections.observableArrayList(
                                new PieChart.Data("Private", totalPrivatePaymentCast),
                                new PieChart.Data("Industry", totalIndustryPaymentCast),
                                new PieChart.Data("Agriculture", totalAgriculturePaymentCast));

                paymentStatistics.setData(pieChartPaymentData);
                consumptionStatistics.setData(pieChartConsumptionData);

                // Fetch totals
                DB.selectSQL("SELECT SUM(fldPrice) FROM tblBill");
                String totalPayments = Functionality.Database.DB.getDisplayData();

                DB.selectSQL("SELECT SUM(fldConsumption) FROM tblReadingCard");
                String totalConsumption = Functionality.Database.DB.getDisplayData();

                // Consumption
                totalWaterConsumptionID.setText(totalConsumption);
                privateWaterConsumptionID.setText(totalPrivateConsumption);
                industrialWaterConsumptionID.setText(totalIndustryConsumption);
                agriculturalWaterConsumptionID.setText(totalAgricultureConsumption);

                // Payment
                totalPaymentsID.setText("$" + totalPayments);
                privatePaymentsID.setText("$" + totalPrivatePayment);
                industrialPaymentsID.setText("$" + totalIndustryPayment);
                agriculturalPaymentsID.setText("$" + totalAgriculturePayment);

                // This is really ugly, but I couldn't get it to work without duplicates.
                // a combined method for picking which chart you want to parse percentiles from
                // would be a lot better.
                for (PieChart.Data data : consumptionStatistics.getData())
                    {
                        data.getNode().addEventHandler(MouseEvent.MOUSE_MOVED,
                                e ->
                                {
                                    double total = 0;
                                    for (PieChart.Data d : consumptionStatistics.getData())
                                        {
                                            total += d.getPieValue();
                                        }
                                    String text = String.format("%.1f%%", 100 * data.getPieValue() / total);
                                    segmentWaterPercentileID.setTranslateX(e.getSceneX() - 55);
                                    segmentWaterPercentileID.setTranslateY(e.getSceneY() - 55);
                                    segmentWaterPercentileID.setText(text);
                                    data.getNode().addEventHandler(MouseEvent.MOUSE_EXITED,
                                            b ->
                                                    segmentWaterPercentileID.setText(""));
                                });
                    }
                for (PieChart.Data data : paymentStatistics.getData())
                    {
                        data.getNode().addEventHandler(MouseEvent.MOUSE_MOVED,
                                e ->
                                {
                                    double total = 0;
                                    for (PieChart.Data d : paymentStatistics.getData())
                                        {
                                            total += d.getPieValue();
                                        }
                                    String text = String.format("%.1f%%", 100 * data.getPieValue() / total);
                                    segmentPaymentPercentileID.setTranslateX(e.getSceneX() - 55);
                                    segmentPaymentPercentileID.setTranslateY(e.getSceneY() - 55);
                                    segmentPaymentPercentileID.setText(text);
                                    data.getNode().addEventHandler(MouseEvent.MOUSE_EXITED,
                                            b ->
                                                    segmentPaymentPercentileID.setText(""));
                                });
                    }
            }
    }